package kurniawan.kukuh.aplikasikatalogdanpenyewaandvd

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterDataCustomer(val dataCust: List<HashMap<String,String>>,
                          val customerActivity: CustomerActivity) : //new
    RecyclerView.Adapter<AdapterDataCustomer.HolderDataCustomer>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataCustomer.HolderDataCustomer {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data_cust,p0,false)
        return HolderDataCustomer(v)
    }

    override fun getItemCount(): Int {
        return dataCust.size
    }

    override fun onBindViewHolder(p0: AdapterDataCustomer.HolderDataCustomer, p1: Int) {
        val data = dataCust.get(p1)
        p0.txIdCust.setText(data.get("id_cust"))
        p0.txNamaCust.setText(data.get("nm_cust"))
        p0.txJnsIdentitas.setText(data.get("jns_identitas"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            var idc = data.get("id_cust").toString()
            customerActivity.showDetail(idc)
        })
        //endNew
    }

    class HolderDataCustomer(v: View) : RecyclerView.ViewHolder(v){
        val txIdCust = v.findViewById<TextView>(R.id.txIdCust4)
        val txNamaCust = v.findViewById<TextView>(R.id.txNamaCust4)
        val txJnsIdentitas = v.findViewById<TextView>(R.id.txJnsIdentitas4)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutCust) //new
    }
}