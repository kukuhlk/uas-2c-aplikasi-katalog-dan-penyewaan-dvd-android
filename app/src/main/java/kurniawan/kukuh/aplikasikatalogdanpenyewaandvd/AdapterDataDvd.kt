package kurniawan.kukuh.aplikasikatalogdanpenyewaandvd

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataDvd(val dataDvd: List<HashMap<String,String>>,
                     val dvdActivity: DVDActivity) : //new
    RecyclerView.Adapter<AdapterDataDvd.HolderDataDvd>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataDvd.HolderDataDvd {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data_dvd, p0, false)
        return HolderDataDvd(v)
    }

    override fun getItemCount(): Int {
        return dataDvd.size
    }

    override fun onBindViewHolder(p0: AdapterDataDvd.HolderDataDvd, p1: Int) {
        val data = dataDvd.get(p1)
        p0.txKdDvd.setText(data.get("kd_dvd"))
        p0.txJdlFilm.setText(data.get("jdl_film"))
        p0.txHrgSewa.setText(data.get("hrg_sewa"))

        //beginNew
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            var kd_dvd = data.get("kd_dvd").toString()
            dvdActivity.showDetail(kd_dvd)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataDvd(v: View) : RecyclerView.ViewHolder(v) {
        val photo = v.findViewById<ImageView>(R.id.imDvd4)
        val txKdDvd = v.findViewById<TextView>(R.id.txKd_Dvd4)
        val txHrgSewa = v.findViewById<TextView>(R.id.txHrgSewa4)
        val txJdlFilm = v.findViewById<TextView>(R.id.txJdlFilm4)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutDVD) //new
    }
}
