package kurniawan.kukuh.aplikasikatalogdanpenyewaandvd

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataSewa(val dataSewa: List<HashMap<String,String>>,
                      val sewaActivity: KonfirmSewaActivity) :
    RecyclerView.Adapter<AdapterDataSewa.HolderDataSewa>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataSewa.HolderDataSewa {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_cart,p0,false)
        return HolderDataSewa(v)
    }

    override fun getItemCount(): Int {
        return dataSewa.size
    }

    override fun onBindViewHolder(p0: AdapterDataSewa.HolderDataSewa, p1: Int) {
        val data = dataSewa.get(p1)
        p0.txJdlSewa.setText(data.get("jdl_film"))
        p0.txLamaSewa.setText(data.get("lama_sewa")+" hari")
        p0.txHrgSewa.setText("Rp "+data.get("jumlah_sewa"))

        //beginNew
        p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

//        p0.cLayout.setOnClickListener(View.OnClickListener {
//            p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
//        })

        //endNew
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataSewa(v: View) : RecyclerView.ViewHolder(v){
        val txJdlSewa = v.findViewById<TextView>(R.id.txJdlSewa)
        val txLamaSewa = v.findViewById<TextView>(R.id.txLamaSewa)
        val txHrgSewa = v.findViewById<TextView>(R.id.txHrgSewa)
        val photo = v.findViewById<ImageView>(R.id.imPost)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutCart)
    }
}