package kurniawan.kukuh.aplikasikatalogdanpenyewaandvd

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataCart(val dataCart: List<HashMap<String,String>>,
                      val cartActivity: CartActivity) :
    RecyclerView.Adapter<AdapterDataCart.HolderDataCart>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataCart.HolderDataCart {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_cart,p0,false)
        return HolderDataCart(v)
    }

    var id_dvd = ""
    var id_cust = ""

    override fun getItemCount(): Int {
        return dataCart.size
    }

    override fun onBindViewHolder(p0: AdapterDataCart.HolderDataCart, p1: Int) {
        val data = dataCart.get(p1)
        p0.txJdlSewa.setText(data.get("jdl_film"))
        p0.txLamaSewa.setText(data.get("lama_sewa")+" hari")
        p0.txHrgSewa.setText("Rp "+data.get("jumlah_sewa"))
        p0.txKdDvd.setText(data.get("kd_dvd"))

        //beginNew
        p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
            var popupMenu = PopupMenu(p0.cLayout.context,p0.cLayout)
            popupMenu.menuInflater.inflate(R.menu.cart_menu,popupMenu.menu)
            popupMenu.setOnMenuItemClickListener {item ->
                when(item.itemId){
                    R.id.itemEdit ->{
                        val intent = Intent(p0.cLayout.context,DetailActivity::class.java)
                        intent.putExtra("id_dvd",data.get("kd_dvd").toString())
                        intent.putExtra("id_cust",data.get("id_cust").toString())
                        p0.cLayout.context.startActivity(intent)
                        true
                    }
                    R.id.itemHapus ->{
                        id_dvd = data.get("kd_dvd").toString()
                        id_cust = cartActivity.id_cust
                        cartActivity.querySewa("delete",id_cust,id_dvd)
                        true
                    }
                }
                false
            }
            popupMenu.show()
        })
        //endNew
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataCart(v: View) : RecyclerView.ViewHolder(v){
        val txJdlSewa = v.findViewById<TextView>(R.id.txJdlSewa)
        val txLamaSewa = v.findViewById<TextView>(R.id.txLamaSewa)
        val txHrgSewa = v.findViewById<TextView>(R.id.txHrgSewa)
        val txKdDvd = v.findViewById<TextView>(R.id.txKdDvd)
        val photo = v.findViewById<ImageView>(R.id.imPost)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayoutCart)
    }
}